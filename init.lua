cookie.fgt = cookie.fgt or {}

cookie.fgt.init = function(verbose)
    fgt = cookie.fgt
    for plugname, data in pairs(fgt) do
        if ((type(data) == "table") and (type(data.init) == "function")) then
            if (verbose) then
                print("Calling fgt." .. plugname .. ".init")
            end
            data.init(verbose)
        end
    end
end
