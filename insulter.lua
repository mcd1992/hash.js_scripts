cookie.fgt.insulter = cookie.fgt.insulter or {}

cookie.fgt.insulter.insult = function(user)
	local insults = cookie.fgt.insulter.insults or {}
	if (#insults < 1) then return "ERROR: cookie.fgt.insulter.insult table is empty!" end
	local i = math.ceil(math.random() * #insults)
	local insult = (string.gsub(insults[i], "%%s", user))
	return (string.gsub(insult, "^%l", string.upper))
end

cookie.fgt.insulter.repairdb = function()
	local t = {}
	for k,v in pairs(cookie.fgt.insulter.insults or {}) do
		if (type(v) == "string") then
			t[v] = true
		end
	end

	local insults = {}
	local i = 1
	for k,v in pairs(t) do
		insults[i] = k
		i = i + 1
	end
	cookie.fgt.insulter.insults = insults
end

cookie.fgt.insulter.init = function(verbose)
	hook.Add("Message", "fgt.insulter.onmessage", function(name, sid, msg)
		if (string.sub(msg, 1, 8) == "!insult ") then
			local target = string.sub(msg, 9)
			if (target == "*") then
				target = "everyone"
			elseif (target == "#") then
					print("Fuck you " .. name)
					target = name
			elseif (target == "me") then
				target = name
			end
			print(cookie.fgt.insulter.insult(target))
		end
	end)
	if (verbose) then
		print("fgt.insulter.onmessage Message hook added.")
	end
end
