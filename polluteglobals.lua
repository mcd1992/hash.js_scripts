cookie.fgt.polluteglobals = cookie.fgt.polluteglobals or {}
cookie.fgt.polluteglobals.chance = 0.90

cookie.fgt.polluteglobals.kek = function()
    if (not kek) then
        kek = setmetatable({}, {
            __tostring = function()
                if (math.random() > cookie.fgt.polluteglobals.chance) then
                    return "xD"
                end
                return ""
            end
        })
    end
end

cookie.fgt.polluteglobals.init = function(verbose)
    for i, func in pairs(cookie.fgt.polluteglobals) do
        if ((i ~= "init") and (type(func) == "function")) then
            func()
        end
    end
end
